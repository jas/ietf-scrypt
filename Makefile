all: draft-josefsson-scrypt-kdf.txt draft-josefsson-scrypt-kdf.html

clean:
	rm *~ draft-josefsson-scrypt-kdf.txt draft-josefsson-scrypt-kdf.html

draft-josefsson-scrypt-kdf.txt: draft-josefsson-scrypt-kdf.xml
	xml2rfc $<

draft-josefsson-scrypt-kdf.html: draft-josefsson-scrypt-kdf.xml
	xml2rfc $< $@
